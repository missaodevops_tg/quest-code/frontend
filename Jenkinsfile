def LABEL_ID = "questcode-${UUID.randomUUID().toString()}"

podTemplate(
    name: 'questcode',
    namespace: 'devops',
    label: LABEL_ID,
    containers: [
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', name: 'docker-container', ttyEnabled: true, workingDir: '/home/jenkins/agent'),
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v3.2.4', name: 'helm-container', ttyEnabled: true)
    ],
    volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')])
{
    def REPOS
    def IMAGE_VERSION
    def IMAGE_POSFIX
    def KUBE_NAMESPACE
    def IMAGE_NAME = "questcode-frontend"
    def ENVIRONMENT
    def GIT_BRANCH
    def CHARTMUSEUM_URL = "http://helm-chartmuseum:8080"
    def HELM_DEPLOY_NAME
    def HELM_CHART_NAME = 'questcode/frontend'
    def INGRESS_HOST

    echo "${LABEL_ID}"

    node(LABEL_ID) {
        stage('Checkout') {
            echo 'Iniciando clone no repositorio'
            REPOS = checkout scm
            GIT_BRANCH = REPOS.GIT_BRANCH

            if (GIT_BRANCH.equals('master')) {
                KUBE_NAMESPACE = 'prod'
                ENVIRONMENT = 'production'
                IMAGE_POSFIX = ""
                INGRESS_HOST = "questcode.org"
            } else if (GIT_BRANCH.equals('develop')) {
                KUBE_NAMESPACE = 'staging'
                ENVIRONMENT = 'staging'
                IMAGE_POSFIX = "-RC"
                INGRESS_HOST = "staging.questcode.org"
            } else {
                def error = "Não existe pipeline para a branch ${GIT_BRANCH}"
                throw new Exception(error)
            }

            HELM_DEPLOY_NAME = KUBE_NAMESPACE + "-" + IMAGE_NAME

            IMAGE_VERSION = sh label: '', returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim() + IMAGE_POSFIX
        }
        stage('Package') {
            container('docker-container') {
                echo 'Iniciando empacotamento com Docker'
                withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'DOCKER_HUB_PASSWORD', usernameVariable: 'DOCKER_HUB_USER')]) {
                    sh label: '', script: """
                        docker login -u '${DOCKER_HUB_USER}' -p '${DOCKER_HUB_PASSWORD}'
                        docker build -t '${DOCKER_HUB_USER}'/${IMAGE_NAME}:${IMAGE_VERSION} . --build-arg NPM_ENV=${ENVIRONMENT}
                        docker push '${DOCKER_HUB_USER}'/${IMAGE_NAME}:${IMAGE_VERSION}
                    """
                }
            }
        }
    }

    node(LABEL_ID) {
        stage('Deploy') {
            container('helm-container') {
                echo 'Iniciando Deploy com helm'
                sh label: '', script: """
                    helm repo add questcode ${CHARTMUSEUM_URL}
                    helm repo update
                """
                try {
                    sh label: '', script: "helm upgrade -n ${KUBE_NAMESPACE} ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION},ingress.hosts[0].host=${INGRESS_HOST},ingress.hosts[0].paths={/}"
                } catch (Exception e) {
                    sh label: '', script: "helm install ${HELM_CHART_NAME} --name-template ${HELM_DEPLOY_NAME} --namespace ${KUBE_NAMESPACE} --set image.tag=${IMAGE_VERSION},ingress.hosts[0].host=${INGRESS_HOST},ingress.hosts[0].paths={/}"
                }
            }
        }
    }
}